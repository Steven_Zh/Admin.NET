﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.Application.Service.Tables.BasicTable.Dto
{
    public interface IBasicTableService
    {
        Task<dynamic> GetBasicTableList();
    }
}
