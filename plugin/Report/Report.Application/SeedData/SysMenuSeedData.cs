﻿using Furion.DatabaseAccessor;
using Furion.Extras.Admin.NET;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.Application.SeedData
{
    /// <summary>
    /// 系统菜单表种子数据
    /// </summary>
    public class SysMenuSeedData : IEntitySeedData<SysMenu>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<SysMenu> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new SysMenu{Id=153658608574533, Pid=0, Pids="[0],", Name="表格示例", Code="table", Type=MenuType.DIR, Icon="area-chart", Router="/table", Component="PageView", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Redirect="", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=1, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850757, Pid=153658608574533, Pids="[0],[153658608574533],", Name="斑马线表格", Code="table_demo", Type=MenuType.MENU, Router="/examples/report/tableDemo", Component="examples/report/tableDemo", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
            };
        }
    }
}
