﻿namespace Report.Application
{
    public interface ISystemService
    {
        string GetDescription();
    }
}