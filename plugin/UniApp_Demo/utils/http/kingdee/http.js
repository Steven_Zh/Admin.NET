import {baseUrl} from '@/utils/http/kingdee/config.js';
export const http = (options) => {
	let commonData =
	{
		'format': 1,
		'useragent': 'ApiClient',
		'rid': (((1+Math.random())*0x10000)|0).toString(16).substring(1),
		'timestamp': new Date().getTime(),
		'v': '1.0',
		'parameters':options.data
	}
	// debugger
	return new Promise((resolve,reject)=>{
		uni.showLoading({
			title:'加载中...',
			mask:true
		})
		// debugger
		uni.request({
			method:options.method,
			url:baseUrl+options.url,
			data:commonData,
			header:{
				'SessionId':uni.getStorageSync('SessionId')
			},
			success:(res)=>{
				resolve(res.data)
			},
			fail:(err)=>{
				reject(err.data)
			},
			complete:()=>{
				uni.hideLoading();
			}
		})
	});
}
export default http