const version = '1.0.0'; //APP版本号
const baseUrl = 'http://localhost/k3Cloud/'; //金蝶云星空地址，不同项目直接替换
//由于应用服务器采用Ngnix进行反向代理，8777这个端口号要和Ngnix里的端口号一样，
// const baseUrl = 'http://localhost:8777/K3cloud/'; //金蝶云星空地址，不同项目直接替换
export {
	version,
	baseUrl
}