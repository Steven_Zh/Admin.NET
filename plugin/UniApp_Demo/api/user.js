import api from '@/utils/http/kingdee/http.js'
import {
  getToken
} from '../utils/util.js'
// let initData = [
// 	'600f89219923f8', // 本机DBID
// 	'Administrator', // 集成用户
// 	'API', // 应用ID
// 	'205149333c7c444fb30b34ceab5eab97', // 应用秘钥
// 	2052 // 语言中心代码
// ]
//金蝶授权登录
export const init = () => {
	// debugger
	return api({
		url:'Kingdee.BOS.WebApi.ServicesStub.AuthService.LoginByAppSecret.common.kdsvc',
		method:'POST',
		data:initData
	})
}
//用户登录
export const login = (jsonData) => {
	// debugger
	const data = [
	    jsonData
	  ]
	return api({
		url:'ZF.Kingdee.ServicePlugIn.WebApi.UserApi.Login,ZF.Kingdee.common.kdsvc',
		method:'POST',
		data:data
	})
}