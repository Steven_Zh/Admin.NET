﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Core.Util
{
    /// <summary>
    /// 小诺分页列表结果
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class XnPageResultSqlSugar<T> where T : new()
    {
        /// <summary>
        /// 替换sqlsugar分页
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public static dynamic SqlSugarPageResult(SqlSugarPagedList<T> page)
        {
            return new
            {
                PageNo = page.PageIndex,
                PageSize = page.PageSize,
                TotalPage = page.TotalPages,
                TotalRows = page.TotalCount,
                Rows = page.Items //.Adapt<List<T>>(),
                //Rainbow = PageUtil.Rainbow(page.PageIndex, page.TotalPages, PageUtil.RAINBOW_NUM)
            };
        }
    }
}
