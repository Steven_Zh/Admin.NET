﻿using Examples.Application.Entity.Forms.OneToMany;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.SeedData
{
    /// <summary>
    /// 患者档案种子数据
    /// </summary>
    public class PatientRecordsSeedData : IEntitySeedData<PatientRecords>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<PatientRecords> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new PatientRecords{Id=142307070898245, 
                    Name="张三", 
                    Age=45,
                    Phone="15226571320",
                    CrowdType = 1,
                    Location = 0,
                    Status=0, 
                    Sort=100 },
                new PatientRecords{Id=142307070898246,
                    Name="李四",
                    Age=46,
                    Phone="15226571320",
                    CrowdType = 1,
                    Location = 0,
                    Status=0,
                    Sort=100 },
                new PatientRecords{Id=142307070898247,
                    Name="王五",
                    Age=47,
                    Phone="15226571320",
                    CrowdType = 1,
                    Location = 0,
                    Status=0,
                    Sort=100 }
            };
        }
    }
}
