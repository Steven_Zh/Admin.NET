﻿using Furion.DatabaseAccessor;
using Furion.Extras.Admin.NET;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.SeedData
{
    public class SysMenuSeedData : IEntitySeedData<SysMenu>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<SysMenu> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new SysMenu{Id=153658608574533, Pid=0, Pids="[0],", Name="表单示例", Code="form", Type=MenuType.DIR, Icon="area-chart", Router="/form", Component="PageView", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Redirect="", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=1, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850757, Pid=153658608574533, Pids="[0],[153658608574533],", Name="患者档案", Code="form_ontomany", Type=MenuType.MENU, Router="/examples/forms/onetomany/index", Component="examples/forms/onetomany/index", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153658608574534, Pid=0, Pids="[0],", Name="金蝶云星空", Code="kingdee", Type=MenuType.DIR, Icon="area-chart", Router="/kingdee", Component="PageView", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Redirect="", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=1, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850758, Pid=153658608574534, Pids="[0],[153658608574534],", Name="执行SQL", Code="kingdee_executeSql", Type=MenuType.MENU, Router="/examples/kingdee/ExcuteSql", Component="examples/kingdee/ExcuteSql", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850759, Pid=153658608574534, Pids="[0],[153658608574534],", Name="查询SQL", Code="kingdee_selectSql", Type=MenuType.MENU, Router="/examples/kingdee/SelectSql", Component="examples/kingdee/SelectSql", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850760, Pid=153658608574534, Pids="[0],[153658608574534],", Name="金蝶云星空项目", Code="kingdee_kingdeeProject", Type=MenuType.MENU, Router="/examples/kingdee/project/index", Component="examples/kingdee/project/index", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153658608574535, Pid=0, Pids="[0],", Name="报表示例", Code="report", Type=MenuType.DIR, Icon="area-chart", Router="/report", Component="PageView", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Redirect="", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=1, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850761, Pid=153658608574535, Pids="[0],[153658608574535],", Name="基本表格", Code="report_basicReport", Type=MenuType.MENU, Router="/examples/report/tableDemo", Component="examples/report/tableDemo", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
            };
        }
    }
}
