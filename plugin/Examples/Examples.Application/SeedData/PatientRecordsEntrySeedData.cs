﻿using Examples.Application.Entity.Forms.OneToMany;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.SeedData
{
    /// <summary>
    /// 患者档案单据体种子数据
    /// </summary>
    public class PatientRecordsEntrySeedData: IEntitySeedData<PatientRecordsEntry>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<PatientRecordsEntry> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new PatientRecordsEntry{
                    Id=152307070898245,
                    PatientId = 142307070898245,
                    Date = DateTime.Now,
                    Before = "以前",
                    Now = "现在",
                    Other = "其他",
                    Status=0,
                    Sort=100 },
                new PatientRecordsEntry{
                    Id=152307070898246,
                    PatientId = 142307070898246,
                    Date = DateTime.Now,
                    Before = "以前",
                    Now = "现在",
                    Other = "其他",
                    Status=0,
                    Sort=100 },
                new PatientRecordsEntry{
                    Id=152307070898247,
                    PatientId = 142307070898247,
                    Date = DateTime.Now,
                    Before = "以前",
                    Now = "现在",
                    Other = "其他",
                    Status=0,
                    Sort=100 }
            };
        }
    }
}
