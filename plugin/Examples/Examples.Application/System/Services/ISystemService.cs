﻿namespace Examples.Application
{
    public interface ISystemService
    {
        string GetDescription();
    }
}