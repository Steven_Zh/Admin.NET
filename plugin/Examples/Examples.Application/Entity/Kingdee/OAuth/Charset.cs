﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Entity.Kingdee.OAuth
{
    public class Charset
    {
        /// <summary>
        /// 
        /// </summary>
        public string BodyName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EncodingName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string HeaderName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string WebName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int WindowsCodePage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string IsBrowserDisplay { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string IsBrowserSave { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string IsMailNewsDisplay { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string IsMailNewsSave { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string IsSingleByte { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public EncoderFallback EncoderFallback { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DecoderFallback DecoderFallback { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string IsReadOnly { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CodePage { get; set; }
    }
}
