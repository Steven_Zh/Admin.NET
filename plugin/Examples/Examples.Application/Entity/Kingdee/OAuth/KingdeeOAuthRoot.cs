﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Entity.Kingdee.OAuth
{
    public class KingdeeOAuthRoot
    {
        /// <summary>
        /// 还剩4天，当前您的软件处于未授权或版本使用期满，为确保您的权益不受侵害，请及时购买正版！
        /// 电话销售：4008 830 830
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MessageCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int LoginResultType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Context Context { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string KDSVCSessionId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FormId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RedirectFormParam { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int FormInputObject { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ErrorStackTrace { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Lcid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AccessToken { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string KdAccessResult { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string IsSuccessByAPI { get; set; }
    }
}
