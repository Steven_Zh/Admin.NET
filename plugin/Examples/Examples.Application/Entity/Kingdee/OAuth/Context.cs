﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Entity.Kingdee.OAuth
{
    public class Context
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserLocale { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LogLocale { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DBid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int DatabaseType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SessionId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<UseLanguagesItem> UseLanguages { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 金蝶软件
        /// </summary>
        public string CustomName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayVersion { get; set; }
        /// <summary>
        /// 恒质测试2021-05-07
        /// </summary>
        public string DataCenterName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserToken { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public CurrentOrganizationInfo CurrentOrganizationInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string IsCH_ZH_AutoTrans { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ClientType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public WeiboAuthInfo WeiboAuthInfo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public UTimeZone UTimeZone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public STimeZone STimeZone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string GDCID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Gsid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TRLevel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ProductEdition { get; set; }
    }
}
