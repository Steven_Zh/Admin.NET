﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Entity.Kingdee.OAuth
{
    public class WeiboAuthInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string WeiboUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NetWorkID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CompanyNetworkID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Account { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AppKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AppSecret { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TokenKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TokenSecret { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Verify { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CallbackUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Charset Charset { get; set; }
    }
}
