﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Entity.Kingdee.OAuth
{
    public class STimeZone
    {
        /// <summary>
        /// 
        /// </summary>
        public int OffsetTicks { get; set; }
        /// <summary>
        /// (UTC+08:00)北京，重庆，香港特别行政区，乌鲁木齐
        /// </summary>
        public string StandardName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Number { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CanBeUsed { get; set; }
    }
}
