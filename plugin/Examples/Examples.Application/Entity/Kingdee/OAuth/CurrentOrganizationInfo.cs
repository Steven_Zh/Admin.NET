﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Entity.Kingdee.OAuth
{
    public class CurrentOrganizationInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AcctOrgType { get; set; }
        /// <summary>
        /// 恒质测试2021-05-07
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<int> FunctionIds { get; set; }
    }
}
