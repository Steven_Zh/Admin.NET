﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Entity.Kingdee.OAuth
{
    public class UseLanguagesItem
    {
        /// <summary>
        /// 
        /// </summary>
        public int LocaleId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LocaleName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string @Alias { get; set; }
    }
}
