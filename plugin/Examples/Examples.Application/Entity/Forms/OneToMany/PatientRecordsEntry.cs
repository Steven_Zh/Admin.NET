﻿using Furion.Extras.Admin.NET;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Entity.Forms.OneToMany
{
    /// <summary>
    /// 患者档案
    /// </summary>
    [SugarTable("PatientRecordsEntry", TableDescription = "患者档案明细")]
    public class PatientRecordsEntry : DEntityBase
    {
        /// <summary>
        /// 员工Id
        /// </summary>
        [SugarColumn(ColumnDescription = "患者ID", IsNullable = false)]
        public long PatientId { get; set; }
        /// <summary>
        /// 诊断日期
        /// </summary>
        [SugarColumn(ColumnDescription = "诊断日期", IsNullable = false)]
        public DateTime Date { get; set; }

        /// <summary>
        /// 既往药物治疗
        /// </summary>
        [SugarColumn(ColumnDescription = "既往药物治疗", IsNullable = false)]
        public string Before { get; set; }

        /// <summary>
        /// 综合治疗方法
        /// </summary>
        [SugarColumn(ColumnDescription = "综合治疗方法", IsNullable = false)]
        public string Now { get; set; }

        /// <summary>
        /// 其他
        /// </summary>
        [SugarColumn(ColumnDescription = "其他", IsNullable = false)]
        public string Other { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        [SugarColumn(ColumnDescription = "状态")]
        public CommonStatus Status { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 排序
        /// </summary>
        [SugarColumn(ColumnDescription = "排序")]
        public int Sort { get; set; }
    }
}
