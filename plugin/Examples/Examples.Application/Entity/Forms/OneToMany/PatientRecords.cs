﻿using Furion.Extras.Admin.NET;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Entity.Forms.OneToMany
{
    /// <summary>
    /// 患者档案
    /// </summary>
    [SugarTable("PatientRecords", TableDescription = "患者档案")]
    public class PatientRecords : DEntityBase
    {
        /// <summary>
        /// 患者名称
        /// </summary>
        [SugarColumn(ColumnDescription = "患者名称", IsNullable = true)]
        public string Name { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        [SugarColumn(ColumnDescription = "年龄", IsNullable = true)]
        public int Age { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        [SugarColumn(ColumnDescription = "电话", IsNullable = true)]
        public string Phone { get; set; }

        /// <summary>
        /// 人群类型
        /// </summary>
        [SugarColumn(ColumnDescription = "人群类型")]
        public int CrowdType { get; set; }

        /// <summary>
        /// 会员地域属性
        /// </summary>
        [SugarColumn(ColumnDescription = "会员地域属性")]
        public int Location { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        [SugarColumn(ColumnDescription = "状态")]
        public CommonStatus Status { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 排序
        /// </summary>
        [SugarColumn(ColumnDescription = "排序")]
        public int Sort { get; set; }
    }
}
