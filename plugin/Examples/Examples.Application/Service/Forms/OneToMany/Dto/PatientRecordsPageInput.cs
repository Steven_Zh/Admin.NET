﻿using Furion.Extras.Admin.NET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Service.Forms.OneToMany.Dto
{
    public class PatientRecordsPageInput : PageInputBase
    {
        /// <summary>
        /// 患者名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 年龄
        /// </summary>
        public virtual int Age { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        public virtual string Phone { get; set; }
    }
}
