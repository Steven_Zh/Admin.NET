﻿using Examples.Application.Const;
using Examples.Application.Entity.Forms.OneToMany;
using Examples.Application.Service.Forms.OneToMany.Dto;
using Examples.Application.Service.Kingdee;
using Examples.Core.Util;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.Extras.Admin.NET;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Serilog;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Examples.Application.Service.Forms.OneToMany
{
    /// <summary>
    /// 患者档案服务
    /// </summary>
    [ApiDescriptionSettings(Name = "PatientRecords", Order = 100)]
    public class PatientRecordsService : IPatientRecordsService, IDynamicApiController, ITransient
    {
        private readonly ISqlSugarRepository<PatientRecords> _patientRecordsRep;
        private readonly SqlSugarClient db; // 核心对象：拥有完整的SqlSugar全部功能
        private readonly IKingdeeService _kingdeeService;
        public PatientRecordsService(ISqlSugarRepository<PatientRecords> patientRecordsRep, IKingdeeService kingdeeService)
        {
            _patientRecordsRep = patientRecordsRep;
            db = _patientRecordsRep.Context;    // 推荐操作
            _kingdeeService = kingdeeService;
        }
        /// <summary>
        /// 分页查询系统应用
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/patientRecords/page")]
        public async Task<dynamic> QueryPatientRecordsList([FromQuery] PatientRecordsPageInput input)
        {
            var lists = await db.Queryable<PatientRecords>()
                .WhereIF(!string.IsNullOrWhiteSpace(input.Name), m => m.Name.Contains(input.Name.Trim()))
                 .WhereIF(!string.IsNullOrWhiteSpace(input.Phone), m => m.Name.Contains(input.Phone.Trim()))
                   .ToPagedListAsync(input.PageNo, input.PageSize);
            return XnPageResultSqlSugar<PatientRecords>.SqlSugarPageResult(lists);
        }
        /// <summary>
        /// 获取患者档案
        /// </summary>
        /// <returns></returns>
        [HttpGet("/patientRecords/list")]
        public async Task<dynamic> GetPatientRecordsList()
        {
            return await _patientRecordsRep.Where(u => u.Status == (int)CommonStatus.ENABLE).ToListAsync();
        }
    }
}
