﻿using Examples.Application.Service.Forms.OneToMany.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Service.Forms.OneToMany
{
    public interface IPatientRecordsService
    {
        Task<dynamic> GetPatientRecordsList();
        Task<dynamic> QueryPatientRecordsList(PatientRecordsPageInput input);
    }
}
