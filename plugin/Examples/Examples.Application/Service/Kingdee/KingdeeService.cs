﻿using Examples.Application.Const;
using Examples.Application.Service.Kingdee.OAuth;
using Furion.DependencyInjection;
using Furion.JsonSerialization;
using Furion.RemoteRequest.Extensions;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Service.Kingdee
{
    public class KingdeeService : IKingdeeService, ISingleton
    {
        private readonly KingdeeOAuthConfig _oauthConfig;
        private readonly IJsonSerializerProvider _jsonSerializer;
        private readonly IMemoryCache _memoryCache;
        private string _kingdeeUrl = "";//金蝶云星空地址
        public KingdeeService(IConfiguration configuration, IJsonSerializerProvider jsonSerializer, IMemoryCache memoryCache)
        {
            _oauthConfig = KingdeeOAuthConfig.LoadFrom(configuration, "oauth:kingdee");
            _kingdeeUrl = _oauthConfig.KdUrl;
            _jsonSerializer = jsonSerializer;
            _memoryCache = memoryCache;
            AnalysisToken();
        }
        public async Task<JObject> DoAction(string url,List<string>content)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["Authorization"] = _memoryCache.Get("kingdee_token");
            var param = BuildParam(content);
            var json = await $"{ _kingdeeUrl}{url}".SetHeaders(dic).SetBody(param, "application/json").SetHttpMethod(HttpMethod.Post).SendAsStringAsync();//返回String
            JObject obj = _jsonSerializer.Deserialize<JObject>(json);
            return obj;
        }
        /// <summary>
        /// 获取金蝶授权Token
        /// </summary>
        /// <returns></returns>
        protected Task<string> GetAccessTokenAsync()
        {
            List<String> list = new List<string>();
            list.Add(_oauthConfig.DbId);
            list.Add(_oauthConfig.User);
            list.Add(_oauthConfig.AppId);
            list.Add(_oauthConfig.AppSecret);
            list.Add(_oauthConfig.Lang);
            var param = BuildParam(list);
            return $"{ _kingdeeUrl}{KingdeeUrlConst.LoginByAppSecret}".SetBody(param, "application/json").SetHttpMethod(HttpMethod.Post).SendAsStringAsync();//返回String
        }
        /// <summary>
        /// 解析金蝶Token，并存储到缓存中
        /// </summary>
        /// <returns></returns>
        protected void AnalysisToken()
        {
            if (_memoryCache.Get("kingdee_token") == null)
            {
                var accessToken = GetAccessTokenAsync().GetAwaiter().GetResult();//异步转同步
                JObject obj = _jsonSerializer.Deserialize<JObject>(accessToken);
                if (Convert.ToInt32(obj["LoginResultType"]) == 1)
                {
                    var cacheOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromDays(10));
                    _memoryCache.Set("kingdee_token", obj["KDSVCSessionId"].ToString(), cacheOptions);
                }
            }
        }
        /// <summary>
        /// 设置请求参数
        /// </summary>
        /// <param name="content">json字符串</param>
        /// <returns></returns>
        protected KingdeeOauthModel BuildParam(List<string> content)
        {
            var param = new KingdeeOauthModel
            {
                format = "1",
                useragent = "ApiClient",
                rid = Guid.NewGuid().ToString().GetHashCode().ToString(),//随机数
                timestamp = DateTime.Now.ToString(),//时间戳
                v = "1.0",
                parameters = content
            };
            return param;
        }
    }
}
