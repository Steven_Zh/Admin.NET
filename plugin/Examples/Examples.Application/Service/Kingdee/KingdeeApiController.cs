﻿using Examples.Application.Const;
using Examples.Application.Service.Kingdee.Dto;
using Examples.Core.Util;
using Furion.DataEncryption;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.JsonSerialization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Service.Kingdee
{
    [ApiDescriptionSettings(Name = "Kingdee", Order = 100)]
    public class KingdeeApiController : IKingdeeApiController, IDynamicApiController, ITransient
    {
        private readonly IKingdeeService _kingdeeService;
        private readonly IJsonSerializerProvider _jsonSerializer;
        public KingdeeApiController(IKingdeeService kingdeeService, IJsonSerializerProvider jsonSerializer)
        {
            _kingdeeService = kingdeeService;
            _jsonSerializer = jsonSerializer;
        }
        [HttpPost("/kingdee/excuteSql")]
        public async Task<JObject> ExcuteSql(SqlModelInput input)
        {
            Log.Information(input.sql);
            List<String> list = new List<string>();
            string sql = DESCEncryptionHepler.Base64Encrypt(_jsonSerializer.Serialize(input));//需要将sql 加密传到金蝶的接口，如果明文传递则报错
            list.Add(sql);
            JObject obj = await _kingdeeService.DoAction(KingdeeUrlConst.ExcuteSql, list);
            return obj;
        }
        [HttpPost("/kingdee/selectSql")]
        public async Task<JObject> SelectSql(SqlModelInput input)
        {
            Log.Information(input.sql);
            List<String> list = new List<String>();

            string sql = DESCEncryptionHepler.Base64Encrypt(_jsonSerializer.Serialize(input));//需要将sql 加密传到金蝶的接口，如果明文传递则报错
            list.Add(sql);
            JObject obj = await _kingdeeService.DoAction(KingdeeUrlConst.SelectSql, list);
            return obj;
        }
    }
}
