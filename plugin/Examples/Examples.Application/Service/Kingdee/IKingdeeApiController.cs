﻿using Examples.Application.Service.Kingdee.Dto;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Service.Kingdee
{
    public interface IKingdeeApiController
    {
        Task<JObject> ExcuteSql(SqlModelInput input);
        Task<JObject> SelectSql(SqlModelInput input);
    }
}
