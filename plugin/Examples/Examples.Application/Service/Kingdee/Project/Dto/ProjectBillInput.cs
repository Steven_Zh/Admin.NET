﻿using Furion.Extras.Admin.NET;
using Furion.Extras.Admin.NET.Service;
using System.ComponentModel.DataAnnotations;

namespace Examples.Application.Service.Kingdee.Project.Dto
{
    /// <summary>
    /// 参数配置
    /// </summary>
    public class ProjectBillPageInput : PageInputBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }
    }

    public class AddProjectBillInput
    {
        /// <summary>
        /// 名称
        /// </summary>
        [Required(ErrorMessage = "参数名称不能为空")]
        public string Name { get; set; }

        /// <summary>
        /// 访问地址
        /// </summary>
        public string KdUrl { get; set; }

        /// <summary>
        /// 数据库ID
        /// </summary>
        public string DbId { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// AppId
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// AppSecret
        /// </summary>
        public string AppSecret { get; set; }

        /// <summary>
        /// Lang
        /// </summary>
        public string Lang { get; set; }
    }

    public class DeleteProjectBillInput : BaseId
    {
    }

    public class UpdateProjectBillInput
    {
        /// <summary>
        /// 应用Id
        /// </summary>
        [Required(ErrorMessage = "应用Id不能为空")]
        public long Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [Required(ErrorMessage = "参数名称不能为空")]
        public string Name { get; set; }

        /// <summary>
        /// 访问地址
        /// </summary>
        public string KdUrl { get; set; }

        /// <summary>
        /// 数据库ID
        /// </summary>
        public string DbId { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// AppId
        /// </summary>
        public string AppId { get; set; }

        /// <summary>
        /// AppSecret
        /// </summary>
        public string AppSecret { get; set; }

        /// <summary>
        /// Lang
        /// </summary>
        public string Lang { get; set; }
    }

    public class QueryProjectBillInput : BaseId
    {
    }
}