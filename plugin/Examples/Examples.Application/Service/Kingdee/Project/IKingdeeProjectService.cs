﻿using Examples.Application.Entity.Kingdee.Project;
using Examples.Application.Service.Kingdee.Project.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Examples.Application.Service.Kingdee.Project
{
    public interface IKingdeeProjectService
    {
        Task AddProjectBill(AddProjectBillInput input);

        Task DeleteProjectBill(DeleteProjectBillInput input);

        Task<ProjectBill> GetProjectBill([FromQuery] QueryProjectBillInput input);

        Task<dynamic> GetProjectBillList();

        Task<dynamic> QueryProjectBillList([FromQuery] ProjectBillPageInput input);

        Task UpdateProjectBill(UpdateProjectBillInput input);
    }
}