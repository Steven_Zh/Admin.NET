﻿using Examples.Application.Entity.Kingdee.Project;
using Examples.Application.Service.Kingdee.Project.Dto;
using Examples.Core.Util;
using Furion.DatabaseAccessor;
using Furion.DatabaseAccessor.Extensions;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.Extras.Admin.NET;
using Furion.FriendlyException;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SqlSugar;
using System.Linq;
using System.Threading.Tasks;

namespace Examples.Application.Service.Kingdee.Project
{
    /// <summary>
    /// 金蝶云星空项目
    /// </summary>
    [ApiDescriptionSettings(Name = "KingdeeProject", Order = 100)]
    public class KingdeeProjectService : IKingdeeProjectService, IDynamicApiController, ITransient
    {
        private readonly ISqlSugarRepository<ProjectBill> _projectBillRep;
        private readonly SqlSugarClient db; // 核心对象：拥有完整的SqlSugar全部功能

        public KingdeeProjectService(ISqlSugarRepository<ProjectBill> projectBillRep)
        {
            _projectBillRep = projectBillRep;
            db = _projectBillRep.Context;    // 推荐操作
        }

        /// <summary>
        /// 分页获取金蝶云星空项目
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/kingdeeProject/page")]
        public async Task<dynamic> QueryProjectBillList([FromQuery] ProjectBillPageInput input)
        {
            var lists = await db.Queryable<ProjectBill>()
                .WhereIF(!string.IsNullOrWhiteSpace(input.Name), m => m.Name.Contains(input.Name.Trim()))
                   .ToPagedListAsync(input.PageNo, input.PageSize);
            return XnPageResultSqlSugar<ProjectBill>.SqlSugarPageResult(lists);
        }

        /// <summary>
        /// 获取金蝶云星空项目列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("/kingdeeProject/list")]
        public async Task<dynamic> GetProjectBillList()
        {
            return await db.Queryable<ProjectBill>().Where(u => u.Status != CommonStatus.DELETED).ToListAsync();
        }

        /// <summary>
        /// 增加金蝶云星空项目
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/kingdeeProject/add")]
        public async Task AddProjectBill(AddProjectBillInput input)
        {
            var isExist = await db.Queryable<ProjectBill>().AnyAsync(u => u.Name == input.Name);
            if (isExist)
                throw Oops.Oh(ErrorCode.D9000);

            var projectBill = input.Adapt<ProjectBill>();
            await db.Insertable(projectBill).ExecuteCommandAsync();
        }

        /// <summary>
        /// 删除金蝶云星空项目
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/kingdeeProject/delete")]
        public async Task DeleteProjectBill(DeleteProjectBillInput input)
        {
            await db.Deleteable<ProjectBill>().Where(it => it.Id == input.Id).ExecuteCommandAsync();
        }

        /// <summary>
        /// 更新金蝶云星空项目
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/kingdeeProject/edit")]
        public async Task UpdateProjectBill(UpdateProjectBillInput input)
        {
            var isExist = await db.Queryable<ProjectBill>().AnyAsync(u => (u.Name == input.Name) && u.Id != input.Id);
            if (isExist)
                throw Oops.Oh(ErrorCode.D9000);

            var projectBill = input.Adapt<ProjectBill>();
            await db.Updateable(projectBill).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommandAsync();
        }

        /// <summary>
        /// 获取金蝶云星空项目
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/kingdeeProject/detail")]
        public async Task<ProjectBill> GetProjectBill([FromQuery] QueryProjectBillInput input)
        {
           return await db.Queryable<ProjectBill>().FirstAsync(it => it.Id == input.Id);
        }
    }
}