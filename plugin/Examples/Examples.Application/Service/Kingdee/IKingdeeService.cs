﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Service.Kingdee
{
    public interface IKingdeeService
    {
        Task<JObject> DoAction(string url,List<string>content);
    }
}
