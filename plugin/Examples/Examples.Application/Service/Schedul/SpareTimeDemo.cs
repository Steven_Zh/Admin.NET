﻿using Furion.DependencyInjection;
using Furion.Logging.Extensions;
using Furion.TaskScheduler;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examples.Application.Service.Schedul
{
    /// <summary>
    /// 定时任务demo
    /// </summary>
    public class SpareTimeDemo:ISpareTimeWorker
    {
        /// <summary>
        /// 3秒后出勤统计
        /// </summary>
        /// <param name="timer">参数</param>
        /// <param name="count">次数</param>
        [SpareTime(3000, "执行Sql", StartNow = true, ExecuteType = SpareTimeExecuteTypes.Parallel)]
        public void ExecSql(SpareTimer timer, long count)
        {
            //创建作用域
            Scoped.Create((factory, scope) =>
            {
                var now = DateTime.Now;
                string log = $"执行计划执行了：{now}；执行了{count}次！";
                Log.Information(log);
            });
        }
    }
}
