﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Examples.Database.Migrations.Migrations
{
    public partial class v102 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "PatientRecordsEntry",
                keyColumn: "Id",
                keyValue: 152307070898245L,
                column: "Date",
                value: new DateTime(2021, 6, 3, 21, 32, 59, 568, DateTimeKind.Local).AddTicks(9602));

            migrationBuilder.UpdateData(
                table: "PatientRecordsEntry",
                keyColumn: "Id",
                keyValue: 152307070898246L,
                column: "Date",
                value: new DateTime(2021, 6, 3, 21, 32, 59, 569, DateTimeKind.Local).AddTicks(6009));

            migrationBuilder.UpdateData(
                table: "PatientRecordsEntry",
                keyColumn: "Id",
                keyValue: 152307070898247L,
                column: "Date",
                value: new DateTime(2021, 6, 3, 21, 32, 59, 569, DateTimeKind.Local).AddTicks(6047));

            migrationBuilder.UpdateData(
                table: "sys_menu",
                keyColumn: "Id",
                keyValue: 153659188850760L,
                columns: new[] { "Code", "Component", "Name", "Pid", "Pids", "Router" },
                values: new object[] { "kingdee_kingdeeProject", "examples/kingdee/project/index", "金蝶云星空项目", 153658608574534L, "[0],[153658608574534],", "/examples/kingdee/project/index" });

            migrationBuilder.InsertData(
                table: "sys_menu",
                columns: new[] { "Id", "Application", "Code", "Component", "CreatedTime", "CreatedUserId", "CreatedUserName", "Icon", "IsDeleted", "Link", "Name", "OpenType", "Permission", "Pid", "Pids", "Redirect", "Remark", "Router", "Sort", "Status", "Type", "UpdatedTime", "UpdatedUserId", "UpdatedUserName", "Visible", "Weight" },
                values: new object[] { 153659188850761L, "busiapp", "report_basicReport", "examples/report/tableDemo", null, 0L, null, null, false, null, "基本表格", 0, null, 153658608574535L, "[0],[153658608574535],", null, null, "/examples/report/tableDemo", 100, 0, 1, null, 0L, null, "Y", 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "sys_menu",
                keyColumn: "Id",
                keyValue: 153659188850761L);

            migrationBuilder.UpdateData(
                table: "PatientRecordsEntry",
                keyColumn: "Id",
                keyValue: 152307070898245L,
                column: "Date",
                value: new DateTime(2021, 6, 2, 16, 28, 26, 191, DateTimeKind.Local).AddTicks(3852));

            migrationBuilder.UpdateData(
                table: "PatientRecordsEntry",
                keyColumn: "Id",
                keyValue: 152307070898246L,
                column: "Date",
                value: new DateTime(2021, 6, 2, 16, 28, 26, 191, DateTimeKind.Local).AddTicks(8473));

            migrationBuilder.UpdateData(
                table: "PatientRecordsEntry",
                keyColumn: "Id",
                keyValue: 152307070898247L,
                column: "Date",
                value: new DateTime(2021, 6, 2, 16, 28, 26, 191, DateTimeKind.Local).AddTicks(8496));

            migrationBuilder.UpdateData(
                table: "sys_menu",
                keyColumn: "Id",
                keyValue: 153659188850760L,
                columns: new[] { "Code", "Component", "Name", "Pid", "Pids", "Router" },
                values: new object[] { "report_basicReport", "examples/report/tableDemo", "基本表格", 153658608574535L, "[0],[153658608574535],", "/examples/report/tableDemo" });
        }
    }
}
