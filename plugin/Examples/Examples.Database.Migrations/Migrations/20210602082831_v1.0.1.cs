﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Examples.Database.Migrations.Migrations
{
    public partial class v101 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProjectBill",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false, comment: "Id主键"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    KdUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DbId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    User = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AppId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    AppSecret = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Lang = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Status = table.Column<int>(type: "int", nullable: false),
                    Sort = table.Column<int>(type: "int", nullable: false),
                    CreatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "创建时间"),
                    UpdatedTime = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true, comment: "更新时间"),
                    CreatedUserId = table.Column<long>(type: "bigint", nullable: false, comment: "创建者Id"),
                    CreatedUserName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "创建者名称"),
                    UpdatedUserId = table.Column<long>(type: "bigint", nullable: false, comment: "修改者Id"),
                    UpdatedUserName = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true, comment: "修改者名称"),
                    IsDeleted = table.Column<bool>(type: "bit", nullable: false, comment: "软删除标记")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectBill", x => x.Id);
                });

            migrationBuilder.UpdateData(
                table: "PatientRecordsEntry",
                keyColumn: "Id",
                keyValue: 152307070898245L,
                column: "Date",
                value: new DateTime(2021, 6, 2, 16, 28, 26, 191, DateTimeKind.Local).AddTicks(3852));

            migrationBuilder.UpdateData(
                table: "PatientRecordsEntry",
                keyColumn: "Id",
                keyValue: 152307070898246L,
                column: "Date",
                value: new DateTime(2021, 6, 2, 16, 28, 26, 191, DateTimeKind.Local).AddTicks(8473));

            migrationBuilder.UpdateData(
                table: "PatientRecordsEntry",
                keyColumn: "Id",
                keyValue: 152307070898247L,
                column: "Date",
                value: new DateTime(2021, 6, 2, 16, 28, 26, 191, DateTimeKind.Local).AddTicks(8496));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProjectBill");

            migrationBuilder.UpdateData(
                table: "PatientRecordsEntry",
                keyColumn: "Id",
                keyValue: 152307070898245L,
                column: "Date",
                value: new DateTime(2021, 5, 31, 19, 29, 55, 163, DateTimeKind.Local).AddTicks(6407));

            migrationBuilder.UpdateData(
                table: "PatientRecordsEntry",
                keyColumn: "Id",
                keyValue: 152307070898246L,
                column: "Date",
                value: new DateTime(2021, 5, 31, 19, 29, 55, 164, DateTimeKind.Local).AddTicks(4025));

            migrationBuilder.UpdateData(
                table: "PatientRecordsEntry",
                keyColumn: "Id",
                keyValue: 152307070898247L,
                column: "Date",
                value: new DateTime(2021, 5, 31, 19, 29, 55, 164, DateTimeKind.Local).AddTicks(4067));
        }
    }
}
