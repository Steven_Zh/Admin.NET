﻿using Admin.NET.Application.Entity.Tables;
using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.SeedData
{
    /// <summary>
    /// 基本表格表种子数据
    /// </summary>
    public class BasicTableSeedData : IEntitySeedData<BasicTableModel>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<BasicTableModel> HasData(DbContext dbContext, Type dbContextLocator)
        {
            return new[]
            {
                new BasicTableModel{Id=142307070898245, Name="test1",NickName = "test1",
                Email = "zhanghao97hao@163.com",Sex="man",Address = "测试地址1"
                , Age=10, Active="Y", Status=0, Sort=100 },
                new BasicTableModel{Id=142307070898246, Name="test2",NickName = "test2",
                Email = "zhanghao97hao@163.com",Sex="woman",Address = "测试地址2"
                , Age=11, Active="Y", Status=0, Sort=100 },
                new BasicTableModel{Id=142307070898247, Name="test3",NickName = "test3",
                Email = "zhanghao97hao@163.com",Sex="man",Address = "测试地址3"
                , Age=12, Active="Y", Status=0, Sort=100 },
                new BasicTableModel{Id=142307070898248, Name="test4",NickName = "test4",
                Email = "zhanghao97hao@163.com",Sex="woman",Address = "测试地址4"
                , Age=13, Active="Y", Status=0, Sort=100 },
                new BasicTableModel{Id=142307070898249, Name="test5",NickName = "test5",
                Email = "zhanghao97hao@163.com",Sex="man",Address = "测试地址5"
                , Age=14, Active="Y", Status=0, Sort=100 },
            };
        }
    }
}
