﻿using Furion.DatabaseAccessor;
using Furion.Extras.Admin.NET;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.SeedData
{
    /// <summary>
    /// 种子数据
    /// </summary>
    public class SysMenuSeedData: IEntitySeedData<SysMenu>
    {
        /// <summary>
        /// 种子数据
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="dbContextLocator"></param>
        /// <returns></returns>
        public IEnumerable<SysMenu> HasData(DbContext dbContext, Type dbContextLocator)
        {
            /*
             Add-Migration v1.0.0 -Context SqlServerDbContext
             update-database v1.0.0 -Context SqlServerDbContext 
             */
            return new[]
            {
                new SysMenu{Id=153658608574533, Pid=0, Pids="[0],", Name="表单示例", Code="form", Type=MenuType.DIR, Icon="profile", Router="/form", Component="PageView", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Redirect="", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=1, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850757, Pid=153658608574533, Pids="[0],[153658608574533],", Name="患者档案", Code="form_ontomany", Type=MenuType.MENU, Router="/examples/forms/onetomany/index", Component="examples/forms/onetomany/index", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850758, Pid=153658608574533, Pids="[0],[153658608574533],", Name="自定义组件", Code="form_myComponents", Type=MenuType.MENU, Router="/examples/jeecg/SelectDemo", Component="examples/jeecg/SelectDemo", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850759, Pid=153658608574533, Pids="[0],[153658608574533],", Name="一对多", Code="form_jeecgVxe", Type=MenuType.MENU, Router="/jeecg/JeecgOrderMainListForJVxeTable", Component="jeecg/JeecgOrderMainListForJVxeTable", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153658608574534, Pid=0, Pids="[0],", Name="金蝶云星空", Code="kingdee", Type=MenuType.DIR, Icon="red-envelope", Router="/kingdee", Component="PageView", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Redirect="", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=1, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850760, Pid=153658608574534, Pids="[0],[153658608574534],", Name="金蝶云星空项目", Code="kingdee_kingdeeProject", Type=MenuType.MENU, Router="/examples/kingdee/project/index", Component="examples/kingdee/project/index", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153658608574535, Pid=0, Pids="[0],", Name="报表示例", Code="report", Type=MenuType.DIR, Icon="pie-chart", Router="/report", Component="PageView", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Redirect="", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=1, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850761, Pid=153658608574535, Pids="[0],[153658608574535],", Name="基本表格", Code="report_basicReport", Type=MenuType.MENU, Router="/examples/report/tableDemo", Component="examples/report/tableDemo", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850762, Pid=153658608574535, Pids="[0],[153658608574535],", Name="排序表格", Code="report_sortReport", Type=MenuType.MENU, Router="/examples/report/sortTable", Component="examples/report/sortTable", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850763, Pid=153658608574535, Pids="[0],[153658608574535],", Name="查询表格", Code="report_searchTable", Type=MenuType.MENU, Router="/examples/report/searchTable", Component="examples/report/searchTable", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153658608574536, Pid=0, Pids="[0],", Name="企业微信", Code="workWechat", Type=MenuType.DIR, Icon="wechat", Router="/workWechat", Component="PageView", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Redirect="", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=1, Status=CommonStatus.ENABLE },
                new SysMenu{Id=153659188850800, Pid=153658608574536, Pids="[0],[153658608574536],", Name="发送消息", Code="workWechat_SendMsg", Type=MenuType.MENU, Router="/examples/wechat/work", Component="examples/wechat/work", Application="busiapp", OpenType=MenuOpenType.NONE, Visible="Y", Weight=MenuWeight.DEFAULT_WEIGHT, Sort=100, Status=CommonStatus.ENABLE },
            };
        }
    }
}
