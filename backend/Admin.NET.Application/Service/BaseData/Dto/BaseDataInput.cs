﻿using Furion.Extras.Admin.NET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.BaseData.Dto
{
    public class BaseDataInput: PageInputBase
    {
        /// <summary>
        /// 用户名称
        /// </summary>
        public virtual string UserName { get; set; }
    }
}
