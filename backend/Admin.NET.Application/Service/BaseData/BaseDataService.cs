﻿using Admin.NET.Application.Entity.SystemEntity;
using Admin.NET.Application.Service.BaseData.Dto;
using Admin.NET.Core.Util;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.Extras.Admin.NET;
using Microsoft.AspNetCore.Mvc;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.BaseData
{
    [ApiDescriptionSettings(Name = "BaseData", Order = 100)]
    public class BaseDataService: IBaseDataService, IDynamicApiController, ITransient
    {
        private readonly ISqlSugarRepository<MySysUser> _userBillRep;
        private readonly SqlSugarClient db; // 核心对象：拥有完整的SqlSugar全部功能
        public BaseDataService(ISqlSugarRepository<MySysUser> userBillRep)
        {
            _userBillRep = userBillRep;
            db = _userBillRep.Context;    // 推荐操作
        }
        /// <summary>
        /// 分页获取金蝶云星空项目
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/baseData/page")]
        public async Task<dynamic> GetUserList([FromQuery] BaseDataInput input)
        {
            string userName = input.UserName == null ? "" : input.UserName.Substring(0, input.UserName.Length - 1);
            var lists = await db.Queryable<MySysUser>()
                .WhereIF(!string.IsNullOrWhiteSpace(input.UserName), m => m.Account.Contains(userName))
                   .ToPagedListAsync(input.PageNo, input.PageSize);
            
            return XnPageResultSqlSugar<MySysUser>.SqlSugarPageResult(lists);
        }
    }
}
