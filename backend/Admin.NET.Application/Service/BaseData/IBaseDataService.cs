﻿using Admin.NET.Application.Service.BaseData.Dto;
using Furion.Extras.Admin.NET;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.BaseData
{
    public interface IBaseDataService
    {
        Task<dynamic> GetUserList([FromQuery] BaseDataInput input);
    }
}
