﻿using Admin.NET.Application.Service.BaseData.Dto;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Forms
{
    public interface IFormService
    {
        Task<dynamic> GetFormEntryList();
    }
}
