﻿using Admin.NET.Application.Entity.SystemEntity;
using Admin.NET.Application.Service.BaseData.Dto;
using Admin.NET.Core.Util;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Microsoft.AspNetCore.Mvc;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Forms
{
    /// <summary>
    /// 表单服务
    /// </summary>
    [ApiDescriptionSettings(Name = "Forms", Order = 100)]
    public class FormService : IFormService, IDynamicApiController, ITransient
    {
        private readonly ISqlSugarRepository<MySysUser> _userBillRep;
        private readonly SqlSugarClient db; // 核心对象：拥有完整的SqlSugar全部功能
        public FormService(ISqlSugarRepository<MySysUser> userBillRep)
        {
            _userBillRep = userBillRep;
            db = _userBillRep.Context;    // 推荐操作
        }
        /// <summary>
        /// 获取表单单据体列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("/forms/entry/list")]
        public async Task<dynamic> GetFormEntryList()
        {
            var lists = await db.Queryable<MySysUser>()
                   .ToPagedListAsync(1, 10);

            return XnPageResultSqlSugar<MySysUser>.SqlSugarPageResult(lists);
        }
    }
}
