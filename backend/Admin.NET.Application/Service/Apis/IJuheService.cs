﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Apis
{
    public interface IJuheService
    {
        string GetDescription();
        /// <summary>
        /// 获取天气
        /// </summary>
        /// <returns></returns>
        Task<JObject> SimpleWeather();
        /// <summary>
        /// 生成二维码
        /// </summary>
        /// <returns></returns>
        Task<JObject> QrCode();
    }
}
