﻿using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.JsonSerialization;
using Furion.RemoteRequest.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Apis
{
    [ApiDescriptionSettings(Name = "Juhe", Order = 100)]
    public class JuheService:IJuheService, IDynamicApiController,ITransient
    {
        private readonly IJsonSerializerProvider _jsonSerializer;
        public JuheService(IJsonSerializerProvider jsonSerializer)
        {
            _jsonSerializer = jsonSerializer;
        }
        [HttpGet("/juhe/getDescription")]
        public string GetDescription()
        {
            return "让 .NET 开发更简单，更通用，更流行。";
        }
        [HttpGet("/juhe/qrCode")]
        public async Task<JObject> QrCode()
        {
            var response = await "http://apis.juhe.cn/qrcode/api?key={key}&type=1&fgcolor=00b7ee&w=90&m=5&text={text}".SetTemplates(new Dictionary<string, object> {
                { "text", "石家庄" },
                { "key", "04efebae71546ab8e8d5259d9d29f7b6" }
            }).GetAsStringAsync();
            JObject obj = _jsonSerializer.Deserialize<JObject>(response);
            return obj;
        }
        [HttpGet("/juhe/simpleWeather")]
        public async Task<JObject> SimpleWeather()
        {
            //var city = UrlHelper.UrlEncode("石家庄");
            var response = await "http://apis.juhe.cn/simpleWeather/query?city={city}&key={key}".SetTemplates(new Dictionary<string, object> {
                { "city", "石家庄" },
                { "key", "690a6ee652f6fcd54139080c82ec8b2a" }
            }).GetAsStringAsync();
            JObject obj = _jsonSerializer.Deserialize<JObject>(response);
            return obj;
        }
    }
}
