﻿using Admin.NET.Application.Const;
using Admin.NET.Application.Entity.Kingdee.OAuth;
using Admin.NET.Application.Entity.Kingdee.Project;
using Furion.DependencyInjection;
using Furion.JsonSerialization;
using Furion.RemoteRequest.Extensions;
using Microsoft.Extensions.Caching.Memory;
using Serilog;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Kingdee.OAuth
{
    public class KingdeeService : IKingdeeService, ISingleton
    {
        private readonly IMemoryCache _memoryCache;
        private readonly IKingdeeOAuthService _kingdeeOAuthService;
        private readonly IJsonSerializerProvider _jsonSerializer;
        public KingdeeService( IMemoryCache memoryCache,
            IKingdeeOAuthService kingdeeOAuthService, 
            IJsonSerializerProvider jsonSerializer)
        {
            _memoryCache = memoryCache;
            _kingdeeOAuthService = kingdeeOAuthService;
            _jsonSerializer = jsonSerializer;
        }
        public async Task<string> DoAction(string url, List<string> content,ProjectBill bill)
        {
            string json = "";
            string kingdee_token = _kingdeeOAuthService.AnalysisTokenByProId(bill);
            if (!string.IsNullOrWhiteSpace(kingdee_token))
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["Authorization"] = kingdee_token;
                var param = _kingdeeOAuthService.BuildParam(content);
                json = await $"{ bill.KdUrl}{url}".SetHeaders(dic).SetBody(param, "application/json").SetHttpMethod(HttpMethod.Post).SendAsStringAsync();//返回String
                Log.Information(json);
            }
            //JObject obj = _jsonSerializer.Deserialize<JObject>(json);
            return json;
        }
    }
}
