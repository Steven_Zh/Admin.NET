﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Kingdee.Dto
{
    public class SqlModelInput
    {
        public long id { get; set; }
        public string sql { get; set; }
    }
}
