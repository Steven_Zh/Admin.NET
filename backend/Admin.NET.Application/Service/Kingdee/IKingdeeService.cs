﻿using Admin.NET.Application.Entity.Kingdee.Project;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Kingdee.OAuth
{
    public interface IKingdeeService
    {
        Task<string> DoAction(string url, List<string> content,ProjectBill bill);
    }
}
