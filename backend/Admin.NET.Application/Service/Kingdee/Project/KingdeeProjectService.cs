﻿using Admin.NET.Application.Const;
using Admin.NET.Application.Entity.Kingdee.Project;
using Admin.NET.Application.Service.Kingdee.Dto;
using Admin.NET.Application.Service.Kingdee.OAuth;
using Admin.NET.Application.Service.Kingdee.Project.Dto;
using Admin.NET.Core.Util;
using Furion.DatabaseAccessor;
using Furion.DatabaseAccessor.Extensions;
using Furion.DependencyInjection;
using Furion.DistributedIDGenerator;
using Furion.DynamicApiController;
using Furion.Extras.Admin.NET;
using Furion.FriendlyException;
using Furion.JsonSerialization;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Serilog;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yitter.IdGenerator;

namespace Admin.NET.Application.Service.Kingdee.Project
{
    /// <summary>
    /// 金蝶云星空项目
    /// </summary>
    [ApiDescriptionSettings(Name = "KingdeeProject", Order = 100)]
    public class KingdeeProjectService : IKingdeeProjectService, IDynamicApiController, ITransient
    {
        private readonly ISqlSugarRepository<ProjectBill> _projectBillRep;
        private readonly SqlSugarClient db; // 核心对象：拥有完整的SqlSugar全部功能
        private readonly IKingdeeService _kingdeeService;
        private readonly IJsonSerializerProvider _jsonSerializer;

        public KingdeeProjectService(ISqlSugarRepository<ProjectBill> projectBillRep,
            IKingdeeService kingdeeService, IJsonSerializerProvider jsonSerializer
            )
        {
            _projectBillRep = projectBillRep;
            db = _projectBillRep.Context;    // 推荐操作
            _kingdeeService = kingdeeService;
            _jsonSerializer = jsonSerializer;
        }

        /// <summary>
        /// 分页获取金蝶云星空项目
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/kingdeeProject/page")]
        public async Task<dynamic> QueryProjectBillList([FromQuery] ProjectBillPageInput input)
        {
            var lists = await db.Queryable<ProjectBill>()
                .WhereIF(!string.IsNullOrWhiteSpace(input.Name), m => m.Name.Contains(input.Name.Trim()))
                   .ToPagedListAsync(input.PageNo, input.PageSize);
            return XnPageResultSqlSugar<ProjectBill>.SqlSugarPageResult(lists);
        }

        /// <summary>
        /// 获取金蝶云星空项目列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("/kingdeeProject/list")]
        public async Task<dynamic> GetProjectBillList()
        {
            return await db.Queryable<ProjectBill>().Where(u => u.Status != CommonStatus.DELETED).ToListAsync();
        }

        /// <summary>
        /// 增加金蝶云星空项目
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/kingdeeProject/add")]
        public async Task AddProjectBill(AddProjectBillInput input)
        {
            var isExist = await db.Queryable<ProjectBill>().AnyAsync(u => u.Name == input.Name);
            if (isExist)
                throw Oops.Oh(ErrorCode.D9000);

            var projectBill = input.Adapt<ProjectBill>();
            projectBill.Id = YitIdHelper.NextId();
            await db.Insertable(projectBill).ExecuteCommandAsync();
        }

        /// <summary>
        /// 删除金蝶云星空项目
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/kingdeeProject/delete")]
        public async Task DeleteProjectBill(DeleteProjectBillInput input)
        {
            await db.Deleteable<ProjectBill>().Where(it => it.Id == input.Id).ExecuteCommandAsync();
        }

        /// <summary>
        /// 更新金蝶云星空项目
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/kingdeeProject/edit")]
        public async Task UpdateProjectBill(UpdateProjectBillInput input)
        {
            var isExist = await db.Queryable<ProjectBill>().AnyAsync(u => (u.Name == input.Name) && u.Id != input.Id);
            if (isExist)
                throw Oops.Oh(ErrorCode.D9000);

            var projectBill = input.Adapt<ProjectBill>();
            //await db.Updateable(projectBill).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommandAsync();
            await projectBill.UpdateAsync(ignoreNullValues: true);
        }

        /// <summary>
        /// 获取金蝶云星空项目
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/kingdeeProject/detail")]
        public async Task<ProjectBill> GetProjectBill([FromQuery] QueryProjectBillInput input)
        {
            return await db.Queryable<ProjectBill>().FirstAsync(it => it.Id == input.Id);
        }
        /// <summary>
        /// 执行sql语句
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/kingdeeProject/excuteSql")]
        public async Task<string> ExcuteSql(SqlModelInput input)
        {
            Log.Information(input.sql);
            ProjectBill bill = db.Queryable<ProjectBill>().First(it => it.Id == input.id);

            List<String> list = new List<string>();
            string sql = DESCEncryptionHepler.Base64Encrypt(_jsonSerializer.Serialize(input));//需要将sql 加密传到金蝶的接口，如果明文传递则报错
            list.Add(sql);
            string json = await _kingdeeService.DoAction(KingdeeUrlConst.ExcuteSql, list,bill);
            return json;
        }
        /// <summary>
        /// 查询sql语句
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/kingdeeProject/selectSql")]
        public async Task<string> SelectSql(SqlModelInput input)
        {
            Log.Information(input.sql);
            List<String> list = new List<String>();
            ProjectBill bill = db.Queryable<ProjectBill>().First(it => it.Id == input.id);
            string sql = DESCEncryptionHepler.Base64Encrypt(_jsonSerializer.Serialize(input));//需要将sql 加密传到金蝶的接口，如果明文传递则报错
            list.Add(sql);
            string json = await _kingdeeService.DoAction(KingdeeUrlConst.SelectSql, list,bill);
            return json;
        }
    }
}
