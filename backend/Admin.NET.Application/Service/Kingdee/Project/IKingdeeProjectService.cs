﻿using Admin.NET.Application.Entity.Kingdee.Project;
using Admin.NET.Application.Service.Kingdee.Dto;
using Admin.NET.Application.Service.Kingdee.Project.Dto;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Kingdee.Project
{
    public interface IKingdeeProjectService
    {
        Task AddProjectBill(AddProjectBillInput input);

        Task DeleteProjectBill(DeleteProjectBillInput input);

        Task<ProjectBill> GetProjectBill([FromQuery] QueryProjectBillInput input);

        Task<dynamic> GetProjectBillList();

        Task<dynamic> QueryProjectBillList([FromQuery] ProjectBillPageInput input);

        Task UpdateProjectBill(UpdateProjectBillInput input);
        Task<string> ExcuteSql(SqlModelInput input);
        Task<string> SelectSql(SqlModelInput input);
    }
}
