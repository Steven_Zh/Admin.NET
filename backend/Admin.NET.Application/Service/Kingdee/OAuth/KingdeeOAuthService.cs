﻿using Admin.NET.Application.Const;
using Admin.NET.Application.Entity.Kingdee.OAuth;
using Admin.NET.Application.Entity.Kingdee.Project;
using Furion.DependencyInjection;
using Furion.JsonSerialization;
using Furion.RemoteRequest.Extensions;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Kingdee.OAuth
{
    public class KingdeeOAuthService : IKingdeeOAuthService, ISingleton
    {
        /*
         * private readonly IMemoryCache _memoryCache;
         * 
         * public KingdeeOAuthService(IConfiguration configuration, IJsonSerializerProvider jsonSerializer, IMemoryCache memoryCache)
         * 
         * _memoryCache = memoryCache;
         * 
         * var cacheOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromDays(10));
         *   
         * _memoryCache.Set("kingdee_token", obj["KDSVCSessionId"].ToString(), cacheOptions);
         * **/
        private readonly KingdeeOAuthConfig _oauthConfig;
        private readonly IJsonSerializerProvider _jsonSerializer;
        private string _kingdeeUrl = "";//金蝶云星空地址
        public KingdeeOAuthService(IConfiguration configuration, IJsonSerializerProvider jsonSerializer)
        {
            _oauthConfig = KingdeeOAuthConfig.LoadFrom(configuration, "oauth:kingdee");
            _kingdeeUrl = _oauthConfig.KdUrl;
            _jsonSerializer = jsonSerializer;
        }
        /// <summary>
        /// 获取金蝶授权Token
        /// </summary>
        /// <returns></returns>
        protected Task<string> GetAccessTokenAsync()
        {
            List<String> list = new List<string>();
            list.Add(_oauthConfig.DbId);
            list.Add(_oauthConfig.User);
            list.Add(_oauthConfig.AppId);
            list.Add(_oauthConfig.AppSecret);
            list.Add(_oauthConfig.Lang);
            var param = BuildParam(list);
            return $"{ _kingdeeUrl}{KingdeeUrlConst.LoginByAppSecret}".SetBody(param, "application/json").SetHttpMethod(HttpMethod.Post).SendAsStringAsync();//返回String
        }
        /// <summary>
        /// 获取金蝶授权Token（通过项目ID）
        /// </summary>
        /// <returns></returns>
        protected Task<string> GetAccessTokenByProIdAsync(ProjectBill bill)
        {
            List<String> list = new List<string>();
            list.Add(bill.DbId);
            list.Add(bill.User);
            list.Add(bill.AppId);
            list.Add(bill.AppSecret);
            list.Add(bill.Lang);
            var param = BuildParam(list);
            return $"{ bill.KdUrl}{KingdeeUrlConst.LoginByAppSecret}".SetBody(param, "application/json").SetHttpMethod(HttpMethod.Post).SendAsStringAsync();//返回String
        }
        /// <summary>
        /// 解析金蝶Token，并存储到缓存中
        /// </summary>
        /// <returns></returns>
        public string AnalysisToken()
        {
            string kingdee_token = "";
            var accessToken = GetAccessTokenAsync().GetAwaiter().GetResult();//异步转同步
            JObject obj = _jsonSerializer.Deserialize<JObject>(accessToken);
            if (Convert.ToInt32(obj["LoginResultType"]) == 1)
            {
                kingdee_token = obj["KDSVCSessionId"].ToString();
            }
            return kingdee_token;
        }
        /// <summary>
        /// 通过项目ID解析金蝶Token，并存储到缓存中
        /// </summary>
        /// <returns></returns>
        public string AnalysisTokenByProId(ProjectBill bill)
        {
            string kingdee_token = "";
            var accessToken = GetAccessTokenByProIdAsync(bill).GetAwaiter().GetResult();//异步转同步
            JObject obj = _jsonSerializer.Deserialize<JObject>(accessToken);
            if (Convert.ToInt32(obj["LoginResultType"]) == 1)
            {
                kingdee_token = obj["KDSVCSessionId"].ToString();
            }
            return kingdee_token;
        }
        /// <summary>
        /// 设置请求参数
        /// </summary>
        /// <param name="content">json字符串</param>
        /// <returns></returns>
        public KingdeeOauthModel BuildParam(List<string> content)
        {
            var param = new KingdeeOauthModel
            {
                format = "1",
                useragent = "ApiClient",
                rid = Guid.NewGuid().ToString().GetHashCode().ToString(),//随机数
                timestamp = DateTime.Now.ToString(),//时间戳
                v = "1.0",
                parameters = content
            };
            return param;
        }
    }
}
