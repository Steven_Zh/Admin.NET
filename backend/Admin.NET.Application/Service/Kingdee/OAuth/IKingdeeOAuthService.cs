﻿using Admin.NET.Application.Entity.Kingdee.OAuth;
using Admin.NET.Application.Entity.Kingdee.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Kingdee.OAuth
{
    public interface IKingdeeOAuthService
    {
        /// <summary>
        /// 解析金蝶Token，并存储到缓存中
        /// </summary>
        /// <returns></returns>
        public string AnalysisToken();
        /// <summary>
        /// 通过项目ID解析金蝶Token，并存储到缓存中
        /// </summary>
        /// <returns></returns>
        public string AnalysisTokenByProId(ProjectBill bill);
        /// <summary>
        /// 设置请求参数
        /// </summary>
        /// <param name="content">json字符串</param>
        /// <returns></returns>
        public KingdeeOauthModel BuildParam(List<string> content);
    }
}
