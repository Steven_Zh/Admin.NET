﻿using Admin.NET.Application.Entity.Tables;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.Extras.Admin.NET;
using Microsoft.AspNetCore.Mvc;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Tables
{
    /// <summary>
    /// 基本表格服务
    /// </summary>
    [ApiDescriptionSettings(Name = "Tables", Order = 100)]
    public class TableService : ITableService, IDynamicApiController, ITransient
    {
        private readonly ISqlSugarRepository<BasicTableModel> _tableRep;
        public TableService(ISqlSugarRepository<BasicTableModel> tableRep)
        {
            _tableRep = tableRep;
        }
        /// <summary>
        /// 获取基本表格数据
        /// </summary>
        /// <returns></returns>
        [HttpGet("/tables/basicTable/list")]
        public async Task<dynamic> GetBasicTableList()
        {
            return await _tableRep.Where(u => u.Status == (int)CommonStatus.ENABLE).ToListAsync();
        }
        /// <summary>
        /// 获取排序表格数据
        /// </summary>
        /// <returns></returns>
        [HttpGet("/tables/sortTable/list")]
        public async Task<dynamic> GetSortTableList()
        {
            return await _tableRep.Where(u => u.Status == (int)CommonStatus.ENABLE).ToListAsync();
        }
    }
}
