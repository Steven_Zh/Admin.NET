﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Services.Dto
{
    /// <summary>
    /// 提单参数
    /// </summary>
    public class AddQuestionInput
    {
        public string Title { get; set; }
        public string Desc { get; set; }
        public string Questiontype { get; set; }
        public Boolean? IsAgent { get; set; }
    }
}
