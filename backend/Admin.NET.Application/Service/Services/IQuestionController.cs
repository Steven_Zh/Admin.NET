﻿using Admin.NET.Application.Service.Services.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.Services
{
    public interface IQuestionController
    {
        /// <summary>
        /// 添加问题
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        Task AddQuestion(AddQuestionInput input);
    }
}
