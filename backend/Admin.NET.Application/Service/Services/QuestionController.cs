﻿using Admin.NET.Application.Entity.Services;
using Admin.NET.Application.Service.Services.Dto;
using Admin.NET.Application.Service.WeChart;
using Admin.NET.Application.Service.WeChart.Dto;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Yitter.IdGenerator;

namespace Admin.NET.Application.Service.Services
{
    [ApiDescriptionSettings(Name = "Question", Order = 100)]
    public class QuestionController: IQuestionController, IDynamicApiController, ITransient
    {
        private readonly ISqlSugarRepository<Question> _qustBillRep;
        private readonly SqlSugarClient db; // 核心对象：拥有完整的SqlSugar全部功能
        private readonly IWorkWeChatMsgService _workWeChatMsgService;

        public QuestionController(ISqlSugarRepository<Question> qustBillRep, IWorkWeChatMsgService workWeChatMsgService)
        {
            _qustBillRep = qustBillRep;
            db = _qustBillRep.Context;    // 推荐操作
            _workWeChatMsgService = workWeChatMsgService;
        }
        /// <summary>
        /// 添加问题
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("/question/add")]
        public async Task AddQuestion(AddQuestionInput input)
        {
            var bill = input.Adapt<Question>();
            bill.Id = YitIdHelper.NextId();
            bill.IsAgent = input.IsAgent == true ? 1 : 0;
            int ret = await db.Insertable(bill).ExecuteCommandAsync();
            if(ret == 1)
            {
                StringBuilder str = new StringBuilder();
                str.Append("您收到一个客户问题，稍后会同步到`邮箱` \r\n");
                str.Append(">**事项详情**\r\n");
                str.Append(">问题标题：<font color=\"info\">" + input.Title+ "</font>\r\n");
                str.Append(">问题描述："+input.Desc+ "\r\n");
                str.Append(">问题类型：" + input.Questiontype + "\r\n");
                TextMsgInput textMsgInput = new TextMsgInput();
                textMsgInput.empCode = "ZhangHao";
                textMsgInput.message = str.ToString();
                await _workWeChatMsgService.SendMarkDownAsync(textMsgInput);
            }
        }
    }
}
