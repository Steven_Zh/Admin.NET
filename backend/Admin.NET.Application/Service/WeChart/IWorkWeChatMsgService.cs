﻿using Admin.NET.Application.Service.WeChart.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Admin.NET.Application.Service.WeChart
{
    /// <summary>
    /// 发送消息服务类
    /// </summary>
    public interface IWorkWeChatMsgService
    {
        /// <summary>
        /// 发送文本消息
        /// </summary>
        /// <param name="textMsgInput"></param>
        /// <returns></returns>
        Task<string> SendTextAsync(TextMsgInput textMsgInput);
        /// <summary>
        /// 发送MarkDown消息
        /// </summary>
        /// <param name="textMsgInput"></param>
        /// <returns></returns>
        Task<string> SendMarkDownAsync(TextMsgInput textMsgInput);
    }
}
