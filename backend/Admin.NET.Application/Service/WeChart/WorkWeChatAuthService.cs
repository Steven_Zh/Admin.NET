﻿using Admin.NET.Application.Entity.WeChat.QYWeChat;
using Admin.NET.Application.Service.WeChart.Dto;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.JsonSerialization;
using Furion.RemoteRequest.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.WeChart
{
    public class WorkWeChatAuthService:IWorkWeChatAuthService, ITransient
    {
        string corpid = "";
        string corpsecret = "";
        private readonly IMemoryCache _memoryCache;
        private readonly IConfiguration _configuration;
        private readonly IJsonSerializerProvider _jsonSerializer;
        public WorkWeChatAuthService(IConfiguration configuration, IJsonSerializerProvider jsonSerializer, IMemoryCache memoryCache)
        {
            _configuration = configuration;
            _jsonSerializer = jsonSerializer;
            _memoryCache = memoryCache;
        }
        /// <summary>
        /// 从缓存获取access_token，如果没有则调用接口获取
        /// </summary>
        /// <returns></returns>
        public string GetAccessToken()
        {
            string accessToken = "";
            if (_memoryCache.Get("access_token") == null)
            {
                accessToken = GetQYAccessTokenAsync().GetAwaiter().GetResult().access_token;
            }
            else
            {
                accessToken = _memoryCache.Get("access_token").ToString();
            }
            return accessToken;
        }
        private async Task<RespModel> GetQYAccessTokenAsync()
        {
            corpid = _configuration["WorkWeChat:corpid"].ToString();
            corpsecret = _configuration["WorkWeChat:corpsecret"].ToString();
            string getAccessTokenUrl = string.Format("https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid={0}&corpsecret={1}", corpid, corpsecret);
            var response = await getAccessTokenUrl.GetAsStringAsync();
            RespModel resp = _jsonSerializer.Deserialize<RespModel>(response);
            var cacheOptions = new MemoryCacheEntryOptions().SetSlidingExpiration(TimeSpan.FromSeconds(resp.expires_in));
            _memoryCache.Set("access_token",resp.access_token, cacheOptions);
            return resp;
        }
    }
}
