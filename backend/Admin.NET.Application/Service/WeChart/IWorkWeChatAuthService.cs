﻿using Admin.NET.Application.Service.WeChart.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.WeChart
{
    /// <summary>
    /// 企业微信操作接口
    /// </summary>
    public interface IWorkWeChatAuthService
    {
        /// <summary>
        /// 从缓存中获取access_token,如果没有则请求接口获取
        /// </summary>
        /// <returns></returns>
        string GetAccessToken();
    }
}
