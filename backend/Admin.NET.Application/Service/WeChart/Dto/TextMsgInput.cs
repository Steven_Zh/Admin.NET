﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.WeChart.Dto
{
    public class TextMsgInput
    {
        public string empCode { get; set; }
        public string message { get; set; }
    }
}
