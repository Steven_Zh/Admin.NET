﻿using Admin.NET.Application.Entity.WeChat.QYWeChat;
using Admin.NET.Application.Service.WeChart.Dto;
using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.JsonSerialization;
using Furion.RemoteRequest.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Service.WeChart
{
    /// <summary>
    /// 企业微信发送各类型消息
    /// </summary>
    [ApiDescriptionSettings(Name = "Work", Order = 100)]
    public class WorkWeChatMsgService: IWorkWeChatMsgService, IDynamicApiController, ITransient
    {
        private readonly IJsonSerializerProvider _jsonSerializer;
        private readonly string messageSendURI = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token={0}";
        //private readonly string uploadTempFileURI = "https://qyapi.weixin.qq.com/cgi-bin/media/upload?access_token={0}&type={1}";
        private IWorkWeChatAuthService _workWeChatAuthService;
        public WorkWeChatMsgService(IJsonSerializerProvider jsonSerializer,IWorkWeChatAuthService workWeChatAuthService)
        {
            _jsonSerializer = jsonSerializer;
            _workWeChatAuthService = workWeChatAuthService;
        }
        [HttpPost("/work/sendText")]
        public async Task<string> SendTextAsync(TextMsgInput textMsgInput)
        {
            string accessToken = _workWeChatAuthService.GetAccessToken();
            string postUrl = "";
            string param = "";
            string postResult = "";
            if (!string.IsNullOrWhiteSpace(accessToken))
            {
                postUrl = string.Format(messageSendURI, accessToken);
                CorpSendText paramData = new CorpSendText(textMsgInput.message);
                foreach (string item in textMsgInput.empCode.Split('|'))
                {
                    paramData.touser = item;
                    param = _jsonSerializer.Serialize(paramData);
                    if (paramData.touser != null)
                    {
                        postResult = await $"{ postUrl}".SetBody(param, "", Encoding.UTF8).SetHttpMethod(HttpMethod.Post).SendAsStringAsync();//返回String
                    }
                    else
                    {
                        postResult = "账号" + paramData.touser + "在OA中不存在!";
                    }
                }
            }
            return postResult;
        }
        [HttpPost("/work/sendMarkDown")]
        public async Task<string> SendMarkDownAsync(TextMsgInput textMsgInput)
        {
            string accessToken = _workWeChatAuthService.GetAccessToken();
            string postUrl = "";
            string param = "";
            string postResult = "";
            if (!string.IsNullOrWhiteSpace(accessToken))
            {
                postUrl = string.Format(messageSendURI, accessToken);
                CorpSendMarkDown paramData = new CorpSendMarkDown(textMsgInput.message);
                foreach (string item in textMsgInput.empCode.Split('|'))
                {
                    paramData.touser = item;
                    param = _jsonSerializer.Serialize(paramData);
                    if (paramData.touser != null)
                    {
                        postResult = await $"{ postUrl}".SetBody(param, "", Encoding.UTF8).SetHttpMethod(HttpMethod.Post).SendAsStringAsync();//返回String
                    }
                    else
                    {
                        postResult = "账号" + paramData.touser + "在OA中不存在!";
                    }
                }
            }
            return postResult;
        }
    }
}
