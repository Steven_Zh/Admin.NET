﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Entity.SystemEntity
{
    [SugarTable("sys_user", TableDescription = "用户表")]
    public class MySysUser
    {
        /// <summary>
        /// 用户表
        /// </summary>
        public MySysUser()
        {
        }

        private System.Int64 _Id;
        /// <summary>
        /// Id主键
        /// </summary>
        public System.Int64 Id { get { return this._Id; } set { this._Id = value; } }

        private System.String _Account;
        /// <summary>
        /// 账号
        /// </summary>
        public System.String Account { get { return this._Account; } set { this._Account = value; } }

        private System.String _Password;
        /// <summary>
        /// 密码
        /// </summary>
        public System.String Password { get { return this._Password; } set { this._Password = value; } }

        private System.String _NickName;
        /// <summary>
        /// 昵称
        /// </summary>
        public System.String NickName { get { return this._NickName; } set { this._NickName = value; } }

        private System.String _Name;
        /// <summary>
        /// 姓名
        /// </summary>
        public System.String Name { get { return this._Name; } set { this._Name = value; } }

        private System.String _Avatar;
        /// <summary>
        /// 头像
        /// </summary>
        public System.String Avatar { get { return this._Avatar; } set { this._Avatar = value; } }

        private System.DateTimeOffset? _Birthday;
        /// <summary>
        /// 生日
        /// </summary>
        public System.DateTimeOffset? Birthday { get { return this._Birthday; } set { this._Birthday = value; } }

        private System.Int32 _Sex;
        /// <summary>
        /// 性别-男_1、女_2
        /// </summary>
        public System.Int32 Sex { get { return this._Sex; } set { this._Sex = value; } }

        private System.String _Email;
        /// <summary>
        /// 邮箱
        /// </summary>
        public System.String Email { get { return this._Email; } set { this._Email = value; } }

        private System.String _Phone;
        /// <summary>
        /// 手机
        /// </summary>
        public System.String Phone { get { return this._Phone; } set { this._Phone = value; } }

        private System.String _Tel;
        /// <summary>
        /// 电话
        /// </summary>
        public System.String Tel { get { return this._Tel; } set { this._Tel = value; } }

        private System.String _LastLoginIp;
        /// <summary>
        /// 最后登录IP
        /// </summary>
        public System.String LastLoginIp { get { return this._LastLoginIp; } set { this._LastLoginIp = value; } }

        private System.DateTimeOffset? _LastLoginTime;
        /// <summary>
        /// 最后登录时间
        /// </summary>
        public System.DateTimeOffset? LastLoginTime { get { return this._LastLoginTime; } set { this._LastLoginTime = value; } }

        private System.Int32 _AdminType;
        /// <summary>
        /// 管理员类型-超级管理员_1、非管理员_2
        /// </summary>
        public System.Int32 AdminType { get { return this._AdminType; } set { this._AdminType = value; } }

        private System.Int32 _Status;
        /// <summary>
        /// 状态-正常_0、停用_1、删除_2
        /// </summary>
        public System.Int32 Status { get { return this._Status; } set { this._Status = value; } }

        private System.DateTimeOffset? _CreatedTime;
        /// <summary>
        /// 创建时间
        /// </summary>
        public System.DateTimeOffset? CreatedTime { get { return this._CreatedTime; } set { this._CreatedTime = value; } }

        private System.DateTimeOffset? _UpdatedTime;
        /// <summary>
        /// 更新时间
        /// </summary>
        public System.DateTimeOffset? UpdatedTime { get { return this._UpdatedTime; } set { this._UpdatedTime = value; } }

        private System.Int64? _CreatedUserId;
        /// <summary>
        /// 创建者Id
        /// </summary>
        public System.Int64? CreatedUserId { get { return this._CreatedUserId; } set { this._CreatedUserId = value; } }

        private System.String _CreatedUserName;
        /// <summary>
        /// 创建者名称
        /// </summary>
        public System.String CreatedUserName { get { return this._CreatedUserName; } set { this._CreatedUserName = value; } }

        private System.Int64? _UpdatedUserId;
        /// <summary>
        /// 修改者Id
        /// </summary>
        public System.Int64? UpdatedUserId { get { return this._UpdatedUserId; } set { this._UpdatedUserId = value; } }

        private System.String _UpdatedUserName;
        /// <summary>
        /// 修改者名称
        /// </summary>
        public System.String UpdatedUserName { get { return this._UpdatedUserName; } set { this._UpdatedUserName = value; } }

        private System.Boolean _IsDeleted;
        /// <summary>
        /// 软删除标记
        /// </summary>
        public System.Boolean IsDeleted { get { return this._IsDeleted; } set { this._IsDeleted = value; } }
    }
}
