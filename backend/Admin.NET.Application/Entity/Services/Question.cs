﻿using Furion.Extras.Admin.NET;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Entity.Services
{
    /// <summary>
    /// 问题记录
    /// </summary>
    [SugarTable("Question", TableDescription = "问题记录")]
    public class Question : DEntityBase
    {
        /// <summary>
        /// 标题
        /// </summary>
        [SugarColumn(ColumnDescription = "标题", IsNullable = true)]
        public string Title { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        [SugarColumn(ColumnDescription = "描述", IsNullable = true)]
        public string Desc { get; set; }

        /// <summary>
        /// 问题类型
        /// </summary>
        [SugarColumn(ColumnDescription = "问题类型", IsNullable = true)]
        public string Questiontype { get; set; }

        /// <summary>
        /// 是否紧急
        /// </summary>
        [SugarColumn(ColumnDescription = "是否紧急", IsNullable = true)]
        public int IsAgent { get; set; }

        /// <summary>
        /// 解决时间
        /// </summary>
        [SugarColumn(ColumnDescription = "解决时间", IsNullable = true)]
        public DateTimeOffset? SolveTime { get; set; }

        /// <summary>
        /// 解决人Id
        /// </summary>
        [SugarColumn(ColumnDescription = "解决人", IsNullable = true)]
        public long? SolveUserId { get; set; }

        /// <summary>
        /// 是否默认激活（Y-是，N-否）,只能有一个系统默认激活
        /// 用户登录后默认展示此系统菜单
        /// </summary>
        [SugarColumn(ColumnDescription = "是否默认激活", Length = 2, IsNullable = true)]
        public string Active { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        [SugarColumn(ColumnDescription = "状态")]
        public CommonStatus Status { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 排序
        /// </summary>
        [SugarColumn(ColumnDescription = "排序")]
        public int Sort { get; set; }
    }
}
