﻿using Furion;
using Furion.DatabaseAccessor;
using Furion.Extras.Admin.NET;
using Furion.Extras.Admin.NET.Service;
using Microsoft.EntityFrameworkCore;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Entity.Kingdee.Project
{
    /// <summary>
    /// 金蝶云星空项目
    /// </summary>
    [SugarTable("ProjectBill", TableDescription = "金蝶云星空项目")]
    public class ProjectBill : DEntityBase
    {
        /// <summary>
        /// 项目名称
        /// </summary>
        [SugarColumn(ColumnDescription = "项目名称")]
        public string Name { get; set; }

        /// <summary>
        /// 访问地址
        /// </summary>
        [SugarColumn(ColumnDescription = "访问地址")]
        public string KdUrl { get; set; }

        /// <summary>
        /// 账套ID
        /// </summary>
        [SugarColumn(ColumnDescription = "账套ID")]
        public string DbId { get; set; }

        /// <summary>
        /// 用户
        /// </summary>
        [SugarColumn(ColumnDescription = "用户")]
        public string User { get; set; }

        /// <summary>
        /// AppId
        /// </summary>
        [SugarColumn(ColumnDescription = "AppId")]
        public string AppId { get; set; }

        /// <summary>
        /// AppSecret
        /// </summary>
        [SugarColumn(ColumnDescription = "AppSecret")]
        public string AppSecret { get; set; }

        /// <summary>
        /// Lang
        /// </summary>
        [SugarColumn(ColumnDescription = "Lang")]
        public string Lang { get; set; }

        /// <summary>
        /// 状态（字典 0正常 1停用 2删除）
        /// </summary>
        [SugarColumn(ColumnDescription = "状态")]
        public CommonStatus Status { get; set; } = CommonStatus.ENABLE;

        /// <summary>
        /// 排序
        /// </summary>
        [SugarColumn(ColumnDescription = "排序")]
        public int Sort { get; set; }
    }
}
