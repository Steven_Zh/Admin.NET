﻿namespace Admin.NET.Application.Entity.WeChat.QYWeChat
{
    public class TempFileRespModel
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public string type { get; set; }
        public string media_id { get; set; }
        public string created_at { get; set; }
    }
}
