﻿namespace Admin.NET.Application.Entity.WeChat.QYWeChat
{
    public class CorpSendMarkDown: CorpSendBase
    {
        private MarkDown _markdown;
        /// <summary>
        /// 要发送的文本，必须小写，企业微信API不识别大写。
        /// </summary>
        public MarkDown markdown
        {
            get { return _markdown; }
            set { this._markdown = value; }
        }


        public CorpSendMarkDown(string content)
        {
            base.msgtype = "markdown";
            this.markdown = new MarkDown
            {
                content = content
            };
        }
    }
}
