﻿namespace Admin.NET.Application.Entity.WeChat.QYWeChat
{
    public class CorpSendFile : CorpSendBase
    {
        private File _file;
        /// <summary>
        /// 要发送的文本，必须小写，企业微信API不识别大写。
        /// </summary>
        public File file
        {
            get { return _file; }
            set { this._file = value; }
        }


        public CorpSendFile(string content)
        {
            base.msgtype = "file";
            this.file = new File
            {
                media_id = content
            };
        }
    }
}
