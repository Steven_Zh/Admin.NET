﻿namespace Admin.NET.Application.Entity.WeChat.QYWeChat
{
    public class File
    {
        private string _media_id;
        /// <summary>
        /// 要发送的文件media_id
        /// </summary>
        public string media_id
        {
            get { return _media_id; }
            set { _media_id = value; }
        }
    }
}
