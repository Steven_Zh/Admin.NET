﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Admin.NET.Application.Const
{
    public class KingdeeUrlConst
    {
        /// <summary>
        /// 金蝶第三方授权登录地址
        /// </summary>
        public const string LoginByAppSecret = "Kingdee.BOS.WebApi.ServicesStub.AuthService.LoginByAppSecret.common.kdsvc";
        /// <summary>
        /// 执行SQL语句
        /// </summary>
        public const string ExcuteSql = "ZF.Kingdee.BaseObject.BaseAbstractWebApiBusinessService.ExecuteSqlForNet,ZF.Kingdee.common.kdsvc";
        /// <summary>
        /// 查询SQL语句
        /// </summary>
        public const string SelectSql = "ZF.Kingdee.BaseObject.BaseAbstractWebApiBusinessService.GetDataBySqlForNet,ZF.Kingdee.common.kdsvc";
    }
}
