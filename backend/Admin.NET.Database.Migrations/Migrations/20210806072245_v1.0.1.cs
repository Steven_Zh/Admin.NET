﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Admin.NET.Database.Migrations.Migrations
{
    public partial class v101 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "sys_online_user");

            migrationBuilder.AlterColumn<int>(
                name: "AdminType",
                table: "sys_user",
                type: "int",
                nullable: false,
                comment: "管理员类型-超级管理员_1、管理员_2、普通账号_3",
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "管理员类型-超级管理员_1、非管理员_2");

            migrationBuilder.UpdateData(
                table: "sys_menu",
                keyColumn: "Id",
                keyValue: 142307070918743L,
                columns: new[] { "Code", "Name", "Permission" },
                values: new object[] { "sys_monitor_mgr_online_user_page", "在线用户查询", "sysOnlineUser:page" });

            migrationBuilder.UpdateData(
                table: "sys_menu",
                keyColumn: "Id",
                keyValue: 153659188850758L,
                columns: new[] { "Component", "Router" },
                values: new object[] { "examples/jeecg/SelectDemo", "/examples/jeecg/SelectDemo" });

            migrationBuilder.InsertData(
                table: "sys_menu",
                columns: new[] { "Id", "Application", "Code", "Component", "CreatedTime", "CreatedUserId", "CreatedUserName", "Icon", "IsDeleted", "Link", "Name", "OpenType", "Permission", "Pid", "Pids", "Redirect", "Remark", "Router", "Sort", "Status", "Type", "UpdatedTime", "UpdatedUserId", "UpdatedUserName", "Visible", "Weight" },
                values: new object[] { 153659188850759L, "busiapp", "form_jeecgVxe", "jeecg/JeecgOrderMainListForJVxeTable", null, null, null, null, false, null, "一对多", 0, null, 153658608574533L, "[0],[153658608574533],", null, null, "/jeecg/JeecgOrderMainListForJVxeTable", 100, 0, 1, null, null, null, "Y", 2 });

            migrationBuilder.InsertData(
                table: "sys_user_role",
                columns: new[] { "SysRoleId", "SysUserId" },
                values: new object[] { 142307070910554L, 142307070910552L });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "sys_menu",
                keyColumn: "Id",
                keyValue: 153659188850759L);

            migrationBuilder.DeleteData(
                table: "sys_user_role",
                keyColumns: new[] { "SysRoleId", "SysUserId" },
                keyValues: new object[] { 142307070910554L, 142307070910552L });

            migrationBuilder.AlterColumn<int>(
                name: "AdminType",
                table: "sys_user",
                type: "int",
                nullable: false,
                comment: "管理员类型-超级管理员_1、非管理员_2",
                oldClrType: typeof(int),
                oldType: "int",
                oldComment: "管理员类型-超级管理员_1、管理员_2、普通账号_3");

            migrationBuilder.CreateTable(
                name: "sys_online_user",
                columns: table => new
                {
                    UserId = table.Column<long>(type: "bigint", nullable: false, comment: "用户Id")
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ConnectionId = table.Column<string>(type: "nvarchar(max)", nullable: true, comment: "连接Id"),
                    LastTime = table.Column<DateTime>(type: "datetime2", nullable: false, comment: "最近时间")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_sys_online_user", x => x.UserId);
                },
                comment: "在线用户表");

            migrationBuilder.UpdateData(
                table: "sys_menu",
                keyColumn: "Id",
                keyValue: 142307070918743L,
                columns: new[] { "Code", "Name", "Permission" },
                values: new object[] { "sys_monitor_mgr_online_user_list", "在线用户列表", "sysOnlineUser:list" });

            migrationBuilder.UpdateData(
                table: "sys_menu",
                keyColumn: "Id",
                keyValue: 153659188850758L,
                columns: new[] { "Component", "Router" },
                values: new object[] { "jeecg/SelectDemo", "/jeecg/SelectDemo" });
        }
    }
}
