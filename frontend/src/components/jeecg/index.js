import JModal from './JModal'
import JDate from './JDate.vue'

// jeecgbiz
import JSelectMultiUser from '../jeecgbiz/JSelectMultiUser.vue'

export default {
  install(Vue) {
    Vue.component(JModal.name, JModal)
    Vue.component('JDate', JDate)

    // jeecgbiz
    Vue.component('JSelectMultiUser', JSelectMultiUser)
  }
}
