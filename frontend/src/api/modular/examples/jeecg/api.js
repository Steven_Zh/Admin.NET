/**
 * 金蝶云星空
 *
 * @author zhangh
 * @date 2021年5月27日17:00:01
 */
 import { axios } from '@/utils/request'

/**
 * 获取字典类型下所有字典，举例，返回格式为：[{code:"M",value:"男"},{code:"F",value:"女"}]
 *
 * @author yubaoshan
 * @date 2020/5/25 02:06
 */
 export function sysDictTypeDropDown (parameter) {
    return axios({
      url: '/sysDictType/dropDown',
      method: 'get',
      params: parameter
    })
  }

/**
 * 获取字典类型下所有字典，举例，返回格式为：[{code:"M",value:"男"},{code:"F",value:"女"}]
 *
 * @author yubaoshan
 * @date 2020/5/25 02:06
 */
 export function getAction (url, parameter) {
  return axios({
    url: url,
    method: 'get',
    params: parameter
  })
}
/**
 * 获取字典类型下所有字典，举例，返回格式为：[{code:"M",value:"男"},{code:"F",value:"女"}]
 *
 * @author yubaoshan
 * @date 2020/5/25 02:06
 */
 export function deleteAction (url, parameter) {
  return axios({
    url: url,
    method: 'get',
    params: parameter
  })
}
/**
 * 获取字典类型下所有字典，举例，返回格式为：[{code:"M",value:"男"},{code:"F",value:"女"}]
 *
 * @author yubaoshan
 * @date 2020/5/25 02:06
 */
 export function downFile (url, parameter) {
  return axios({
    url: url,
    method: 'get',
    params: parameter
  })
}
/**
 * 获取字典类型下所有字典，举例，返回格式为：[{code:"M",value:"男"},{code:"F",value:"女"}]
 *
 * @author yubaoshan
 * @date 2020/5/25 02:06
 */
 export function getFileAccessHttpUrl (url, parameter) {
  return axios({
    url: url,
    method: 'get',
    params: parameter
  })
}
export function ajaxGetDictItems(url, parameter) {
  return axios({
    url: url,
    method: 'get',
    params: parameter
  })
}
export function getDictItemsFromCache(url, parameter) {
  return axios({
    url: url,
    method: 'get',
    params: parameter
  })
}
