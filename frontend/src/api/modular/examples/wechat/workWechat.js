/**
 * 企业微信
 *
 * @author zhangh
 * @date 2021年5月27日17:00:01
 */
 import { axios } from '@/utils/request'

 /**
  * 发送文本消息
  *
  * @author zhangh
  * @date 2021年7月4日23:40:01
  */
 export function sendText (parameter) {
     return axios({
         url: '/work/sendText',
         method: 'post',
         data: parameter
     })
 }
