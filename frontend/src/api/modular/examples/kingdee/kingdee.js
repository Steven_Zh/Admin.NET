/**
 * 金蝶云星空
 *
 * @author zhangh
 * @date 2021年5月27日17:00:01
 */
import { axios } from '@/utils/request'

/**
 * 执行SQL语句
 *
 * @author zhangh
 * @date 2021年5月27日17:00:01
 */
export function executeSql (parameter) {
    return axios({
        url: '/kingdeeProject/excuteSql',
        method: 'post',
        data: parameter
    })
}
/**
 * 查询SQL语句
 *
 * @author zhangh
 * @date 2021年5月31日19:00:01
 */
 export function selectSql (parameter) {
    return axios({
        url: '/kingdeeProject/selectSql',
        method: 'post',
        data: parameter
    })
}
