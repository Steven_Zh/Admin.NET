/**
 * 一对多表单
 *
 * @author zhangh
 * @date 2021年6月16日10:23:01
 */
 import { axios } from '@/utils/request'

 /**
 * 获取表单单据体列表
 *
 * @author yubaoshan
 * @date 2020年7月9日15:05:01
 */
export function getFormEntryList () {
    return axios({
      url: '/forms/entry/list',
      method: 'get'
    })
  }
 /**
  * 患者档案列表
  *
  * @author zhangh
  * @date 2021年5月16日10:23:01
  */
 export function getPatientRecordsList (parameter) {
     return axios({
         url: '/patientRecords/list',
         method: 'get',
         params: parameter
     })
 }
 /**
 * 新增系统应用
 *
 * @author yubaoshan
 * @date 2020年7月9日15:05:01
 */
export function sysAppAdd (parameter) {
    return axios({
        url: '/sysApp/add',
        method: 'post',
        data: parameter
    })
}
/**
 * 编辑系统应用
 *
 * @author yubaoshan
 * @param parameter
 * @returns {*}
 */
export function sysAppEdit (parameter) {
return axios({
    url: '/sysApp/edit',
    method: 'post',
    data: parameter
})
}
/**
 * 删除系统应用
 *
 * @author yubaoshan
 * @date 2020年7月9日15:05:01
 */
export function sysAppDelete (parameter) {
return axios({
    url: '/sysApp/delete',
    method: 'post',
    data: parameter
})
}
/**
 * 设为默认应用
 *
 * @author yubaoshan
 * @date 2020年7月9日15:05:01
 */
export function sysAppSetAsDefault (parameter) {
return axios({
    url: '/sysApp/setAsDefault',
    method: 'post',
    data: parameter
})
}
/**
 * 修改应用状态
 *
 * @author zuohuaijun
 * @date 2021年1月1日
 */
export function sysAppChangeStatus (parameter) {
return axios({
    url: '/sysApp/changeStatus',
    method: 'post',
    data: parameter
})
}
