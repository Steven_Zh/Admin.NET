import Vue from 'vue'
import Vuex from 'vuex'
import api from "@/api/api"
import MinCache from '@/common/util/MinCache.js'
import cashe from '@/common/util/storage.js'
import {
	ACCESS_TOKEN,
	USER_NAME,
	USER_INFO
} from "@/common/util/constants"

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		token: '',
		userid: '',
		username: '',
		realname: '',
		welcome: '',
		avatar: ''
	},
	mutations: {
		SET_TOKEN: (state, token) => {
			state.token = token
		},
		SET_NAME: (state, {
			username,
			realname,
			welcome
		}) => {
			state.username = username
			state.realname = realname
			state.welcome = welcome
		},
		SET_AVATAR: (state, avatar) => {
			state.avatar = avatar
		}
	},
	actions: {
		/*
		{
		    "success":true,
		    "code":200,
		    "message":"请求成功",
		    "data":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySWQiOjE0MjMwNzA3MDkxMDU1MSwiVGVuYW50SWQiOm51bGwsIkFjY291bnQiOiJzdXBlckFkbWluIiwiTmFtZSI6Iui2hee6p-euoeeQhuWRmCIsIlN1cGVyQWRtaW4iOjEsImlhdCI6MTYyNTg4MTE5MiwibmJmIjoxNjI1ODgxMTkyLCJleHAiOjE2MjU5Njc1OTIsImlzcyI6ImRpbG9uIiwiYXVkIjoiZGlsb24ifQ.D7TFvZZDftdZpy9K8TfuGQc8Zx9IdwJu4BIAMiGDd28",
		    "extras":null,
		    "timestamp":1625881192315
		}
		*/
		// 登录
		mLogin({
			commit
		}, userInfo) {
			return new Promise((resolve, reject) => {
				api.login(userInfo).then(response => {
					if (response.data.code == 200) {
						// uni.setStorageSync(ACCESS_TOKEN, response.data.data);//将Token存入缓存中
						cashe(ACCESS_TOKEN,response.data.data,2000);//设置token33分钟过期
						uni.setStorageSync(USER_INFO, response.data);
						commit('SET_TOKEN', response.data.data)
						// commit('SET_AVATAR', userInfo.avatar)
						// commit('SET_NAME', {
						// 	username: userInfo.username,
						// 	realname: userInfo.realname
						// })
						resolve(response)
					} else {
						resolve(response)
					}
				}).catch(error => {
					console.log("catch===>response", response)
					reject(error)
				})
			})
		},
		//手机号登录
		PhoneLogin({
			commit
		}, userInfo) {
			return new Promise((resolve, reject) => {
				api.phoneNoLogin(userInfo).then(response => {
					if (response.data.code == 200) {
						const result = response.data.result
						const userInfo = result.userInfo
						uni.setStorageSync(ACCESS_TOKEN, result.token);
						uni.setStorageSync(USER_INFO, userInfo);
						commit('SET_TOKEN', result.token)
						commit('SET_NAME', {
							username: userInfo.username,
							realname: userInfo.realname
						})
						commit('SET_AVATAR', userInfo.avatar)
						resolve(response)
					} else {
						reject(response)
					}
				}).catch(error => {
					reject(error)
				})
			})
		},
		// 登出
		Logout({
			commit,
			state
		}) {
			return new Promise((resolve) => {
				let logoutToken = state.token;
				commit('SET_TOKEN', '')
				uni.removeStorageSync(ACCESS_TOKEN)
				api.logout(logoutToken).then(() => {
					resolve()
				}).catch(() => {
					resolve()
				})
			})
		},

	},
	getters: {
		token: state => state.token,
		username: state => {
			state.userid = uni.getStorageSync(USER_INFO).username;
			return state.username
		},
		nickname: state => {
			state.userid = uni.getStorageSync(USER_INFO).realname;
			return state.user.realname
		},
		avatar: state => {
			state.userid = uni.getStorageSync(USER_INFO).avatar;
			return state.user.avatar
		},
		userid: state => {
			state.userid = uni.getStorageSync(USER_INFO).id;
			return state.userid
		},
	}
})
